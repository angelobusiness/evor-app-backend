const express = require("express");
const cors = require("cors");

const app = express();

let config;
if (process.env.NODE_ENV === 'production') {
    config = require("./conf.production");
} else {
    if (process.env.NODE_ENV !== 'development') {
        process.env.NODE_ENV = 'development';
    }
    config = require("./conf.development");
}
process.env.PORT = config.PORT;


var corsOptions = {
    origin: config.CORS_ORIGIN
};

app.use(cors(corsOptions));

// parse requests of content-type - application/json
app.use(express.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(express.urlencoded({ extended: true }));

const db = require("./app/models");
// require('./app/db.initials')(db);


// simple route
app.get("/", (req, res) => {
    res.json({ message: "Welcome to evor-app." });
});

if (process.env.NODE_ENV === 'production') {
    app.use(require('./app/routes'));
}
else {
    app.use('/api', require('./app/routes'));
}

// set port, listen for requests
const PORT = process.env.PORT;
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}.`);
});

// app._router.stack.forEach(function (r) {
//     if (r.route && r.route.path) {
//         console.log(r.route.path)
//     }
// })
// console.log(app._router.stack);