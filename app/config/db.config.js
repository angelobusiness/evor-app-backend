const config = require("./database.json");

const env = process.env.NODE_ENV || 'development';

module.exports = {
  HOST: config[env].host,
  USER: config[env].username,
  PASSWORD: config[env].password,
  DB: config[env].database,
  dialect: config[env].dialect,
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  }
};