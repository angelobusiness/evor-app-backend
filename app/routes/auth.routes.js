const { authJwt, verifySignUp, verifyRecaptcha } = require("../middleware");
const controller = require("../controllers/auth.controller");
var router = require('express').Router();

router.use(function (req, res, next) {
    res.header(
        "Access-Control-Allow-Headers",
        "x-access-token, Origin, Content-Type, Accept"
    );
    next();
});

//  "/api/auth/test"
router.get("/test", (req, res) => {
    res.json({ message: "api auth test" });
});

//  "/api/auth/signup"
router.post(
    "/signup",
    [verifySignUp.checkEmailExists],
    controller.signup
);

//  "/api/auth/signup/verify-code"
router.post(
    "/signup/verify-code",
    [authJwt.verifyToken],
    controller.signupVerifyCode
);

//  "/api/auth/signup/set-country"
router.post(
    "/signup/set-country",
    [authJwt.verifyToken],
    controller.signupSetCountry
);

//  "/api/auth/signin"
router.post(
    "/signin",
    controller.signin
);

//  "/api/auth/signin/verify-email-captcha"
router.post(
    "/signin/verify-email-captcha",
    [verifyRecaptcha.checkResponseTokenIsValid],
    controller.signinGenerateCodeAndAuthToken
);

//  "/api/auth/signin/generate-new-code"
router.post(
    "/signin/generate-new-code",
    [authJwt.verifyToken],
    controller.generateNewCode
);

//  "/api/auth/signin/set-new-password"
router.post(
    "/signin/set-new-password",
    [authJwt.verifyToken],
    controller.signinSetNewPassword
);

module.exports = router;