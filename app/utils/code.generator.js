const CodeGenerator = require('node-code-generator');
const generator = new CodeGenerator();
const pattern = '######';
const howMany = 1;
const options = {};

exports.generate = function () {
    // Generate an array of random unique codes according to the provided pattern:
    const codes = generator.generateCodes(pattern, howMany, options);
    return codes[0];
};
