'use strict';

module.exports = (sequelize, DataTypes) => {
    const User = sequelize.define('User', {
        email: DataTypes.STRING(100),
        pass: DataTypes.STRING(255),
        active: DataTypes.BOOLEAN,
        verify_code: DataTypes.STRING(255),
        code_created_at: DataTypes.DATE,
        code_expires_at: DataTypes.DATE,
        country: DataTypes.STRING(255)
    });

    // User.associate = function (models) {
    // associations can be defined here
    // };

    return User;
};