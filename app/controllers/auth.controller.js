const db = require("../models");
const config = require("../config/auth.config");
const mailer = require('../utils/mailer');
const code = require('../utils/code.generator');
const User = db.user;

var jwt = require("jsonwebtoken");
var bcrypt = require("bcryptjs");

const addSeconds = (date, seconds) => {
    return new Date(date.getTime() + seconds * 1000);
};

exports.signup = (req, res) => {
    // Save User to Database
    User.create({
        email: req.body.email,
        pass: bcrypt.hashSync(req.body.password, 8),
        verify_code: code.generate()
    })
        .then(user => {
            mailer.send({
                to: user.email,
                subject: "Verify your account",
                html: `Your verification code is: ${user.verify_code}`
            });

            var scope = "registration";
            var token = jwt.sign({ id: user.id, scope: scope }, config.secret, {
                expiresIn: 86400 // 24 hours
            });

            res.status(200).send({
                email: user.email,
                scope: scope,
                accessToken: token
            });
        })
        .catch(err => {
            res.status(500).send({ message: err.message });
        });
};

exports.signupVerifyCode = (req, res) => {
    User.update({ active: 1 }, {
        where: { id: req.userId, verify_code: req.body.code }
    })
        .then(user => {
            if (!user) {
                return res.status(404).send({ message: "Verification code is invalid." });
            }

            res.send({ message: "Verified!" });
        })
        .catch(err => {
            res.status(500).send({ message: err.message });
        });
};

exports.signupSetCountry = (req, res) => {
    User.update({ country: req.body.country }, {
        where: { id: req.userId }
    })
        .then(user => {
            if (!user) {
                return res.status(404).send({ message: "Unable to save the country." });
            }

            res.send({ message: "Country saved!" });
        })
        .catch(err => {
            res.status(500).send({ message: err.message });
        });
};

exports.signin = (req, res) => {
    User.findOne({
        where: { email: req.body.email }
    })
        .then(user => {
            if (!user) {
                return res.status(404).send({ message: "User not found." });
            }

            var passwordIsValid = bcrypt.compareSync(
                req.body.password,
                user.pass
            );

            if (!passwordIsValid) {
                return res.status(401).send({
                    accessToken: null,
                    message: "Invalid Password!"
                });
            }

            var token = jwt.sign({ id: user.id }, config.secret, {
                expiresIn: 86400 // 24 hours
            });

            res.status(200).send({
                id: user.id,
                email: user.email,
                accessToken: token
            });
        })
        .catch(err => {
            res.status(500).send({ message: err.message });
        });
};

exports.signinGenerateCodeAndAuthToken = (req, res) => {
    User.findOne({
        where: { email: req.body.email }
    })
        .then(user => {
            if (!user) {
                return res.status(404).send({ message: "User not found." });
            }

            let vCode = code.generate();
            let expiresInSeconds = 300;
            user.update({
                verify_code: vCode,
                code_created_at: new Date(),
                code_expires_at: addSeconds(new Date(), expiresInSeconds),
            })
                .then(affected => {
                    if (!affected) {
                        return res.status(404).send({ message: "Unable to update user." });
                    }

                    mailer.send({
                        to: user.email,
                        subject: "Verify setting new password",
                        html: `Your verification code is: ${vCode}`
                    });

                    var scope = "login_reset_pass";
                    var token = jwt.sign({ id: user.id, scope: scope }, config.secret, {
                        expiresIn: 86400 // 24 hours
                    });

                    res.status(200).send({
                        email: user.email,
                        scope: scope,
                        timeout: expiresInSeconds,
                        accessToken: token
                    });
                })
                .catch(err => {
                    res.status(500).send({ message: err.message });
                });
        });
};

exports.generateNewCode = (req, res) => {
    User.findOne({
        where: { id: req.userId }
    })
        .then(user => {
            if (!user) {
                return res.status(404).send({ message: "User not found." });
            }

            let vCode = code.generate();
            let expiresInSeconds = 300;
            user.update({
                verify_code: vCode,
                code_created_at: new Date(),
                code_expires_at: addSeconds(new Date(), expiresInSeconds),
            })
                .then(affected => {
                    if (!affected) {
                        return res.status(404).send({ message: "Unable to update user." });
                    }

                    mailer.send({
                        to: user.email,
                        subject: "Verify setting new password",
                        html: `Your verification code is: ${vCode}`
                    });

                    res.status(200).send({
                        timeout: expiresInSeconds
                    });
                })
                .catch(err => {
                    res.status(500).send({ message: err.message });
                });
        });
};

exports.signinSetNewPassword = (req, res) => {
    User.findOne({
        where: { id: req.userId, verify_code: req.body.code }
    })
        .then(user => {
            // if no user found based on code then wrong code was given
            if (!user) {
                return res.status(404).send({ message: "Unable to verify the code." });
            }

            // if code has already expired.
            if (user.code_expires_at <= new Date()) {
                return res.status(404).send({ message: "Verification code already expired." });
            }

            user.update({
                pass: bcrypt.hashSync(req.body.password, 8)
            })
                .then(affected => {
                    if (!affected) {
                        return res.status(404).send({ message: "Unable to update user." });
                    }

                    res.send({ message: "New password saved!" });
                })
                .catch(err => {
                    res.status(500).send({ message: err.message });
                });
        });

};
