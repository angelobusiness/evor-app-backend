module.exports = function (db) {
    db.sequelize.sync().then(() => {
        console.log('Resync Db');

        var bcrypt = require("bcryptjs");
        var pass = bcrypt.hashSync('password', 8);
        db.user.findOrCreate({
            where: { id: 1, email: 'test@example.com' },
            defaults: { id: 1, email: 'test@example.com', pass: pass }
        }).then((result) => {
            console.log('result', result);
        });
    });
};
