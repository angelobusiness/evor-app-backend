'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Users', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.BIGINT(20).UNSIGNED,
      },
      email: {
        allowNull: false,
        defaultValue: '',
        type: Sequelize.STRING(100),
        unique: true
      },
      pass: {
        allowNull: false,
        defaultValue: '',
        type: Sequelize.STRING(255)
      },
      active: {
        allowNull: false,
        defaultValue: 0,
        type: Sequelize.BOOLEAN
      },
      verify_code: {
        allowNull: false,
        defaultValue: '',
        type: Sequelize.STRING(255)
      },
      country: {
        allowNull: false,
        defaultValue: '',
        type: Sequelize.STRING(255)
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Users');
  }
};