'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn(
        'Users',
        'code_created_at',
        {
          type: Sequelize.DATE,
          after: 'verify_code'
        }
      ),
      queryInterface.addColumn(
        'Users',
        'code_expires_at',
        {
          type: Sequelize.DATE,
          after: 'code_created_at'
        }
      ),
    ]);
  },

  down: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn('Users', 'code_created_at'),
      queryInterface.removeColumn('Users', 'code_expires_at'),
    ]);
  }
};
