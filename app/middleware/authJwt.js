const jwt = require("jsonwebtoken");
const config = require("../config/auth.config.js");
const db = require("../models");
const User = db.user;

verifyToken = (req, res, next) => {
    let token = req.headers["x-access-token"];

    if (!token) {
        return res.status(403).send({
            message: "No token provided!"
        });
    }

    jwt.verify(token, config.secret, (err, decoded) => {
        if (err) {
            return res.status(401).send({
                message: "Unauthorized!"
            });
        }
        req.userId = decoded.id;
        next();
    });
};

testVerify = () => {
    var user_id = 'reg-1';
    var token = jwt.sign({ id: user_id }, config.secret, {
        expiresIn: 86400 // 24 hours
    });

    jwt.verify(token, config.secret, (err, decoded) => {
        if (err) {
            console.log('got error on checking token...', err);
        }
        else {
            var decoded_id = decoded.id;
            console.log('token:', token);
            console.log('same?', user_id == decoded_id, { decoded_id, user_id });
        }
    });
};

const authJwt = {
    verifyToken: verifyToken,
    testVerify: testVerify,
    // isAdmin: isAdmin,
    // isModerator: isModerator,
    // isModeratorOrAdmin: isModeratorOrAdmin
};
module.exports = authJwt;