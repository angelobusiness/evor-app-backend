const authJwt = require("./authJwt");
const verifySignUp = require("./verifySignUp");
const verifyRecaptcha = require("./verifyRecaptcha");

module.exports = {
    authJwt,
    verifySignUp,
    verifyRecaptcha
};