const db = require("../models");
const User = db.user;

checkEmailExists = (req, res, next) => {
    // Email
    User.findOne({
        where: {
            email: req.body.email
        }
    }).then(user => {
        if (user) {
            res.status(400).send({
                message: "Email is already in use!"
            });
            return;
        }

        next();
    });
};

const verifySignUp = {
    checkEmailExists: checkEmailExists
};

module.exports = verifySignUp;