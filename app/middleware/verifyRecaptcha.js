const config = require("../config/recaptcha.config");
const axios = require("axios");

checkResponseTokenIsValid = (req, res, next) => {
    axios.request({
        method: 'post',
        url: config.api_uri,
        params: {
            secret: config.secret,
            response: req.body.recaptcha_response
        }
    })
        .then(response => {
            console.log("typeof response", typeof response);
            if (response.data.success === false) {
                console.log("recaptcha response", response.data);
                res.status(400).send({
                    message: "Unable to verify Recaptcha"
                });
                return;
            }

            next();
        });
};

const verifyRecaptcha = {
    checkResponseTokenIsValid
};

module.exports = verifyRecaptcha;