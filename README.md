## Creating the app

```bash
mkdir evor-app-backend && cd evor-app-backend
npm install express sequelize mysql2 body-parser cors jsonwebtoken bcryptjs --save
```

## Install MySql

```bash
sudo apt update
sudo apt install -y mysql-server
sudo mysql_secure_installation
# mysql pass = @m11Bmysql
mysql -u root -p
GRANT ALL PRIVILEGES ON *.* TO 'newuser'@'localhost' IDENTIFIED BY 'jL/Bq=Hm';
mysql -u newuser -p
CREATE DATABASE evor_app CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
mysql -u newuser -p evor_app < src/users.sql
```

## Access Mysql DB in the terminal

```bash
mysql -u newuser -p evor_app
# password = jL/Bq=Hm
show tables;
\q # to quit
```

## Start Mysql

Stop Ampps if it's currently running. Then Open the WSL (Ubuntu) terminal to run the commands below.

```bash
which mysql
sudo service mysql start
# password = ' '
sudo service mysql status # to make sure it's running
```

## Stop Mysql

```bash
sudo service mysql stop
# password = ' '
sudo service mysql status # to make sure it's stopped
```

## Start the server

```bash
nodemon server.js
```

## Install nodemon & use nodemon

```bash
npm install -g nodemon
nodemon server.js
# Cntrl + C to stop
```

## Sequelize Migration

```bash
npx sequelize db:migrate
```

