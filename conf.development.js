module.exports = {
    PORT: process.env.PORT || 8080,
    CORS_ORIGIN: "*"
};